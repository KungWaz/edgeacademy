module.exports = function(grunt) {
	'use strict';
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        version: '<%= pkg.version %>',
        name: '<%= pkg.name %>',
        dist: './dist',
        src: './src',
        tmp: './tmp',
        assets: './assets',
        chartjs: '<%= assets %>/lib/chartjs',
        angularjs: '<%= assets %>/lib/angularjs',
        angulartics: '<%= assets %>/lib/angulartics',
        angularfire: '<%= assets %>/lib/angularfire',
        angularchart: '<%= assets %>/lib/angularchart',
        angularlocalstorage: '<%= assets %>/lib/angularlocalstorage',
		angularscrollglue: '<%= assets %>/lib/angularscrollglue',
        bootstrap: '<%= assets %>/lib/bootstrap',
        clean: {
            options: { force: true },
            build: ['<%= dist %>']
        },
        less: {
            development: {
                files: [
                    {
                        '<%= dist %>/public/css/<%= name %>.<%= version %>.css':[
                            '<%= bootstrap %>/css/bootstrap.min.css',
                            '<%= bootstrap %>/css/bootstrap-theme.min.css',
                            '<%= angularchart %>/css/angular-chart.css',
                            '<%= src %>/routes/main.less'
                        ]
                    }
                ]
            },
            production: {
                options: {
                    cleancss: true
                },
                files: [
                    {
                        '<%= dist %>/public/css/<%= name %>.<%= version %>.min.css':[
                            '<%= bootstrap %>/css/bootstrap.min.css',
                            '<%= bootstrap %>/css/bootstrap-theme.min.css',
                            '<%= angularchart %>/css/angular-chart.css',
                            '<%= src %>/routes/main.less'
                        ]
                    }
                ]
            }
        },
        jshint: {
            files: [
                'gruntfile.js',
                '<%= src %>/**/*.js'
            ],
            options: {
                // defaults defined in .jshintrc
                jshintrc : '.jshintrc'
            }
        },
        html2js: {
            options: {
                base: '<%= src %>'
            },
            main: {
                src: ['src/**/!(index).html'],
                dest: '<%= tmp %>/templates.js'
            }
        },
        concat: {
            options: {
                separator: "/*! <%= name %> <%= version %> <%= grunt.template.today('dd-mm-yyyy') %> */\n"
            },
            development: {
                files: [
                    {
                        '<%= dist %>/public/js/<%= name %>.<%= version %>.js': [
                            '<%= chartjs %>/*.js',
                            '<%= angularjs %>/angular-main.js',
                            '<%= angularjs %>/!(angular-main).js',
                            '<%= angularfire %>/*.js',
                            '<%= angulartics %>/*.js',
                            '<%= angularlocalstorage %>/*.js',
							'<%= angularscrollglue %>/*.js',
                            '<%= angularchart %>/js/*.js',
                            '<%= tmp %>/templates.js',
                            '<%= src %>/app.js',
                            '<%= src %>/**/!(app).js'
                        ]
                    }
                ]
            }
        },
        uglify: {
            options: {
                banner: "/*! <%= name %> <%= version %> <%= grunt.template.today('dd-mm-yyyy') %> */\n",
                mangle: false
            },
            production: {
                files: [
                    {
                        '<%= dist %>/public/js/<%= name %>.<%= version %>.min.js': [
                            '<%= chartjs %>/*.js',
                            '<%= angularjs %>/angular-main.js',
                            '<%= angularjs %>/!(angular-main).js',
                            '<%= angularfire %>/*.js',
                            '<%= angulartics %>/*.js',
                            '<%= angularlocalstorage %>/*.js',
							'<%= angularscrollglue %>/*.js',
                            '<%= angularchart %>/js/*.js',
                            '<%= tmp %>/templates.js',
                            '<%= src %>/app.js',
                            '<%= src %>/**/!(app).js'
                        ]
                    }
                ]
            }
        },
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        src: [
							'app.js',
							'gruntfile.js',
							'package.json',
							'robots.txt',
							'humans.txt',
							'sitemap.xml'
						],
                        dest: '<%= dist %>'
                    },
                    {
                        expand: true,
                        src: ['img/**/*'],
                        dest: '<%= dist %>/public',
                        cwd: '<%= assets %>'
                    },
                    {
                        expand: true,
                        src: ['fonts/*'],
                        dest: '<%= dist %>/public',
                        cwd: '<%= bootstrap %>'
                    },
                    {
                        expand: true,
                        src: ['index.html'],
                        dest: '<%= dist %>',
                        cwd: '<%= src %>'
                    }
                ]
            }
        },
        replace: {
			main: {
				options: {
					patterns: [
						{
							match: 'faviconIncludeReplaceToken',
							replacement: '/public/img/favicon.ico'
						},
						{
							match: 'name',
							replacement: '<%= name %>'
						},
						{
							match: 'trackingId',
							replacement: 'UA-59787286-1'
						}
					]
				},
				files: [
					{
						expand: true,
						flatten: true,
						src: [
							'<%= dist %>/public/js/<%= name %>.<%= version %>.js',
							'<%= dist %>/public/js/<%= name %>.<%= version %>.min.js'
						],
						dest: '<%= dist %>/public/js'
					},
					{
						expand: true,
						flatten: true,
						src: ['<%= dist %>/index.html'],
						dest: '<%= dist %>/'
					}
				]
			},
            development: {
                options: {
                    patterns: [
                        {
                            match: 'jsIncludeReplaceToken',
                            replacement: '/public/js/<%= name %>.<%= version %>.js'
                        },
                        {
                            match: 'cssIncludeReplaceToken',
                            replacement: '/public/css/<%= name %>.<%= version %>.css'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= dist %>/public/js/<%= name %>.<%= version %>.js'],
                        dest: '<%= dist %>/public/js'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= dist %>/index.html'],
                        dest: '<%= dist %>/'
                    }
                ]
            },
            production: {
                options: {
                    patterns: [
                        {
                            match: 'jsIncludeReplaceToken',
                            replacement: '/public/js/<%= name %>.<%= version %>.min.js'
                        },
                        {
                            match: 'cssIncludeReplaceToken',
                            replacement: '/public/css/<%= name %>.<%= version %>.min.css'
                        }
                    ]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= dist %>/public/js/<%= name %>.<%= version %>.min.js'],
                        dest: '<%= dist %>/public/js'
                    },
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= dist %>/index.html'],
                        dest: '<%= dist %>/'
                    }
                ]
            }
        }
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-html2js');

    // Grunt tasks
    grunt.registerTask('default', [
        'clean',
		'jshint',
        'html2js',
        'uglify:production',
        'less:production',
        'copy',
		'replace:main',
        'replace:production'
    ]);

    grunt.registerTask('dev', [
        'clean',
		'jshint',
        'html2js',
        'concat:development',
        'less:development',
        'copy',
		'replace:main',
        'replace:development'
    ]);
};
