// let's create a re-usable factory that generates the $firebaseAuth instance
angular.module('auth.service',[])
    .factory('Auth', ['$window', '$firebaseAuth', '$firebase', 'FBURL', 'Online',
		function($window, $firebaseAuth, $firebase, FBURL, Online) {
			'use strict';

			var firebaseObj = new Firebase(FBURL),
				Auth = $firebaseAuth(firebaseObj);

				var authObj = {
					register: function(user) {
						return Auth.$createUser({
							email: user.email,
							password: user.password
						});
					},
					createProfile: function(uid, user) {
						var profile = {
							username: user.username,
							email: user.email
						},
						profilesRef = $firebase(firebaseObj.child('profiles'));

						return profilesRef.$set(uid, profile);
					},
					resetPassword: function(user) {
						return Auth.$resetPassword({
							email: user.email
						});
					},
					removeUser: function(user) {
						return Auth.$removeUser({
							email: user.email,
							password: user.password
						});
					},
					removeProfile: function(userId) {
						var profileRef = $firebase(firebaseObj.child('profiles').child(userId)).$asObject();

						return profileRef.$remove();
					},
					logIn: function(user) {
						return Auth.$authWithPassword({
							email: user.email,
							password: user.password
						});
					},
					logOut: function() {
						Auth.$unauth();
					},
					getAuth: function() {
						return Auth;
					},
					isLoggedIn: function() {
						return !!authObj.user.provider;
					},
					user: {}
				};

			Auth.$onAuth(function(authData) {
				if(authData) {
					console.log('Logged in as:', authData.uid);

					angular.copy(authData, authObj.user);

					var profileRef = $firebase(firebaseObj.child('profiles').child(authObj.user.uid));

					authObj.user.profile = profileRef.$asObject(authObj.user.uid);

					Online.initOnlineStatus(authObj.user.uid);

					if($window.ga) {
						$window.ga('set', '&uid', authObj.user.uid);
					}
				} else {
					console.log('Logged out');

					if(authObj.user && authObj.user.profile) {
						authObj.user.profile.$destroy();
					}

					angular.copy({}, authObj.user);

					if($window.ga) {
						$window.ga('set', '&uid', null);
					}
				}
			});

			return authObj;
		}
	]);