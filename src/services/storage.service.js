angular.module('storage.service',[])
    .provider('appDataStoreSrvc', function() {
		'use strict';

        var _data = []; // our data storage array

        var _set = function(what, val) {
            if(angular.isDefined(what)) {
                _data[what] = (angular.isDefined(val)) ? angular.copy(val) : undefined;
                return true;
            } else {
                return false;
            }
        }; // end _set

        // Provider method for set
        this.set = _set;

        // service methods
        this.$get = [function() {
            return {
                set : _set, // end set

                get : function(what) {
                    if(angular.isDefined(what) && (what in _data)) {
						return angular.copy(_data[what]);
					} else {
						return undefined;
					}
                }, // end get

                del : function(what){
                    if(angular.isDefined(what)) {
                        var i = _data.indexOf(what);
                        if(i >= 0) {
							return _data.splice(i,1);
						}
                    }
                    return false;
                }, // end del

                clear : function() {
                    _data = [];
                } // end clear
            };
        }]; // end $get
    }); // end appDataStoreSrvc / storage-services