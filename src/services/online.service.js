// let's create a re-usable service that generates the online instance
angular.module('online.service',[])
    .service('Online', ['$firebase', 'FBURL',
		function($firebase, FBURL) {
			'use strict';

			var firebaseObj = new Firebase(FBURL),
				amOnline = new Firebase(FBURL + '/.info/connected');

			this.initOnlineStatus = function(userId) {
				var userRef = new Firebase(FBURL + '/presence/' + userId);

				amOnline.on('value', function(snapshot) {
					if(snapshot.val()) {
						console.log('register online');
						userRef.onDisconnect().remove();
						userRef.set(true);
					}
				});
			};

			this.getOnlineUsers = function() {
				return $firebase(firebaseObj.child('presence')).$asArray();
			};

			this.setOffline = function(userId) {
				var userRef = new Firebase(FBURL + '/presence/' + userId);

				userRef.remove();
			};
		}
	]);