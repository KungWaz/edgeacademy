// let's create a re-usable service that generates the messaging instance
angular.module('message.service',[])
    .service('Message', ['$firebase', 'FBURL', 'localStorageService',
		function($firebase, FBURL, localStorageService) {
			'use strict';

			var firebaseObj = new Firebase(FBURL),
				messageObj = {
					limit: 25,
					stepVal: 0
				};

			this.getMessages = function(limit) {
				if(!limit) {
					// Set the default limit
					limit = messageObj.limit;
				}

				var messagesRef = $firebase(firebaseObj.child('messages').limitToLast(limit));

				return messagesRef.$asArray();
			};

			this.getMessageCount = function(limit) {
				if(!limit) {
					// Set the default limit (one hour)
					limit = 60;
				}

				var countRef = $firebase(firebaseObj.child('messageCount').limitToLast(limit));

				return countRef.$asArray();
			};

			this.incrementMessageCount = function(dateObj) {
				// Round to nearest minute
				var roundVal = 1000 * 60,
					timeStamp = new Date(Math.round(dateObj.getTime() / roundVal) * roundVal).getTime(),
					// Get counter ref
					countRef = $firebase(firebaseObj.child('messageCount').child(timeStamp));

				// Increment the message count by 1
				countRef.$transaction(function(currentCount) {
					// Initial value for counter.
					if(!currentCount) {
						return 1;
					}
					// Return undefined to abort transaction.
					if(currentCount < 0) {
						return;
					}
					// Increment the count by 1.
					return currentCount + 1;
				}).then(function(snapshot) {
					if(snapshot === null) {
						// Handle aborted transaction.
						console.log('Increment failed');
					} else {
						console.log('Increment successful');
					}
				}, function(error) {
					console.log("Error:", error);
				});
			};

			this.getMessageLimit = function() {
				return messageObj.limit;
			};

			this.setMessageLimit = function(val) {
				messageObj.limit = parseInt(val, 10);

				localStorageService.set('msgLimit', messageObj.limit);
			};

			this.getStepVal = function() {
				return messageObj.stepVal;
			};

			this.setStepVal = function(val) {
				messageObj.stepVal = parseInt(val, 10);

				localStorageService.set('stepVal', messageObj.stepVal);
			};
		}
	]);