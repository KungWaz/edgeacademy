/* jshint strict: false, maxlen:200 */

// Declare app level module which depends on controllers, directives, filters, and services
angular.module('@@name', [
    // Angular dependencies
    'ngRoute',

    // Other angular dependencies
    'firebase',
	'chart.js',
    'LocalStorageModule',
	'luegg.directives',

    // Webtracking using google analytics in angular - http://luisfarzati.github.io/angulartics/
    'angulartics',
    'angulartics.google.analytics',

    // HTML templates in $templateCache
    'templates-main',

    // Application specific dependencies
    'storage.service',
	'auth.service',
	'message.service',
	'online.service',
    'main.controller',
    'sign-in.controller',
    'register.controller',
    'reset-password.controller',
	'profile.controller',
    'chat.controller',
	'statistics.controller',
    'string-to-color.directive',
	'timestamp-to-string.filter'
])
.config(['$routeProvider', '$analyticsProvider', 'localStorageServiceProvider',
    function($routeProvider, $analyticsProvider, localStorageServiceProvider) {
        $routeProvider
			.when('/sign-in', {
                templateUrl: 'routes/sign-in/sign-in.html',
                controller: 'SignInCtrl as signInCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $waitForAuth returns a promise so the resolve waits for it to complete
						return Auth.getAuth().$waitForAuth();
					}]
				}
            })
            .when('/register', {
                templateUrl: 'routes/register/register.html',
                controller: 'RegisterCtrl as registerCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $waitForAuth returns a promise so the resolve waits for it to complete
						return Auth.getAuth().$waitForAuth();
					}]
				}
            })
            .when('/reset-password', {
                templateUrl: 'routes/reset-password/reset-password.html',
                controller: 'ResetPasswordCtrl as resetPasswordCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $waitForAuth returns a promise so the resolve waits for it to complete
						return Auth.getAuth().$waitForAuth();
					}]
				}
            })
			.when('/statistics', {
				templateUrl: 'routes/statistics/statistics.html',
				controller: 'StatisticsCtrl as statisticsCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						return Auth.getAuth().$waitForAuth();
					}]
				}
			})
			.when('/profile', {
				templateUrl: 'routes/profile/profile.html',
				controller: 'ProfileCtrl as profileCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						return Auth.getAuth().$requireAuth();
					}]
				}
			})
            .when('/chat', {
                templateUrl: 'routes/chat/chat.html',
                controller: 'ChatCtrl as chatCtrl',
				resolve: {
					// controller will not be loaded until $waitForAuth resolves
					// Auth refers to our $firebaseAuth wrapper in the example above
					'currentAuth': ['Auth', function(Auth) {
						// $requireAuth returns a promise so the resolve waits for it to complete
						// If the promise is rejected, it will throw a $stateChangeError (see above)
						return Auth.getAuth().$requireAuth();
					}]
				}
            })
            .otherwise({
                redirectTo: '/sign-in'
            });

        // turn off automatic tracking
        $analyticsProvider.virtualPageviews(false);

        localStorageServiceProvider.setPrefix('@@name');
        localStorageServiceProvider.setNotify(true, true);
    }
])
// Firebase data URL
.constant('FBURL', 'https://edgeacademy.firebaseio.com')
.run(['$rootScope', '$location', 'localStorageService', 'Message',
	function($rootScope, $location, localStorageService, Message) {
		$rootScope.$on('$routeChangeError', function(event, next, previous, error) {
			// We can catch the error thrown when the $requireAuth promise is rejected
			// and redirect the user back to the home page
			if(error === 'AUTH_REQUIRED') {
				console.log('Auth required');

				$location.path('/sign-in');
			}
		});

		var msgLimit = parseInt(localStorageService.get('msgLimit'), 10);

		if(msgLimit) {
			Message.setMessageLimit(msgLimit);
		}

		var stepVal = parseInt(localStorageService.get('stepVal'), 10);

		if(stepVal) {
			Message.setStepVal(stepVal);
		}
	}
]);