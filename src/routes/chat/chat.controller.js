/* jshint maxlen:200 */
angular.module('chat.controller', [])
    .controller('ChatCtrl', ['$rootScope', '$scope', '$analytics', 'Auth', 'Message',
        function($rootScope, $scope, $analytics, Auth, Message) {
            'use strict';

            var chatCtrl = this;

			$analytics.pageTrack('/chat');

			$rootScope.scrollGlue = true;

            chatCtrl.message = '';
            chatCtrl.messages = Message.getMessages();

            chatCtrl.sendMessage = function() {
				var userProfile = Auth.user.profile;

                if(userProfile && userProfile.username && chatCtrl.message) {
                    var dateObj = new Date();

                    chatCtrl.messages.$add({
						uid: Auth.user.uid,
                        timestamp: dateObj.getTime(),
                        username: userProfile.username,
                        text: chatCtrl.message
                    }).then(function() {
                        console.log('Message sent: ' + chatCtrl.message);

                        Message.incrementMessageCount(dateObj);

						$analytics.eventTrack('Chat', {
                            category: 'Send',
                            label: 'User ID - ' + Auth.user.uid,
                            value: Auth.user.uid
                        });

                        chatCtrl.message = '';
                    }, function(error) {
                        console.log('Error:', error);
                    });
                }
            };

			$scope.$on('$destroy', function() {
				chatCtrl.messages.$destroy();
			});
        }
    ]);