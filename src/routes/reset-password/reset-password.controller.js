/* jshint maxlen:200 */
angular.module('reset-password.controller', [])
    .controller('ResetPasswordCtrl', ['$rootScope', '$location', '$analytics', 'Auth', 'currentAuth',
        function($rootScope, $location, $analytics, Auth, currentAuth) {
            'use strict';

            var signInCtrl = this;

			if(currentAuth !== null) {
				$location.path('chat');
			}

			$analytics.pageTrack('/reset-password');

			$rootScope.scrollGlue = false;

            signInCtrl.user = {};

            signInCtrl.resetPassword = function() {
                var email = signInCtrl.user.email;

				if(email) {
					Auth.resetPassword({
						email: email
					}).then(function() {
						console.log('Password reset email sent successfully!');

						$analytics.eventTrack('Reset password', {
							category: 'Reset',
							label: 'E-mail - ' + email,
							value: email
						});

						$location.path('sign-in');
					}).catch(function(error) {
						console.error('Error: ', error);
					});
				}
            };
        }
    ]);