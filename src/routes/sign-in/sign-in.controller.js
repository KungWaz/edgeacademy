/* jshint maxlen:200 */
angular.module('sign-in.controller', [])
    .controller('SignInCtrl', ['$rootScope', '$location', '$analytics', 'Auth', 'currentAuth',
        function($rootScope, $location, $analytics, Auth, currentAuth) {
            'use strict';

            var signInCtrl = this;

			if(currentAuth !== null) {
				$location.path('chat');
			}

			$analytics.pageTrack('/sign-in');

			$rootScope.scrollGlue = false;

            signInCtrl.user = {};
            signInCtrl.hasAuthError = false;

            signInCtrl.signIn = function() {
                var email = signInCtrl.user.email,
                    password = signInCtrl.user.password;

				Auth.logIn({
                    email: email,
                    password: password
                }).then(function(authData) {
                    console.log('Logged in as:', authData.uid);

					$analytics.eventTrack('Sign in', {
                        category: 'Sign in',
                        label: 'User ID - ' + authData.uid,
                        value: authData.uid
                    });

                    signInCtrl.hasAuthError = false;
                    $location.path('chat');
                }).catch(function(error) {
                    console.error('Error: ', error);

                    signInCtrl.hasAuthError = true;
                });
            };

            signInCtrl.register = function() {
                $location.path('register');
            };

            signInCtrl.resetPassword = function() {
                $location.path('reset-password');
            };
        }
    ]);