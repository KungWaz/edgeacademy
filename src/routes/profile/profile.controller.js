/* jshint maxlen:200 */
angular.module('profile.controller', [])
    .controller('ProfileCtrl', ['$rootScope', '$location', '$analytics', 'Auth', 'Online', 'Message',
        function($rootScope, $location, $analytics, Auth, Online, Message) {
            'use strict';

            var profileCtrl = this;

			$analytics.pageTrack('/profile');

			$rootScope.scrollGlue = false;

            profileCtrl.user = Auth.user;
			profileCtrl.removePassword = '';

			profileCtrl.msgLimit = Message.getMessageLimit();
			profileCtrl.msgLimits = [10, 15, 20, 25, 30, 35, 40, 45, 50];

			profileCtrl.setMsgLimit = function() {
				$analytics.eventTrack('Profile', {
					category: 'Message limit',
					label: 'Limit - ' + profileCtrl.msgLimit,
					value: profileCtrl.msgLimit
				});

				Message.setMessageLimit(profileCtrl.msgLimit);
			};

			profileCtrl.removeUser = function() {
				var email = profileCtrl.user.profile.email,
					password = profileCtrl.removePassword;

				if(profileCtrl.dataValid()) {
					Auth.removeProfile(Auth.user.uid).then(function() {
						Online.setOffline(Auth.user.uid);

						Auth.removeUser({
							email: email,
							password: password
						}).then(function() {
							console.log('User successfully removed!');

							$analytics.eventTrack('Profile', {
								category: 'Remove',
								label: 'E-mail - ' + email,
								value: email
							});

							$location.path('sign-in');
						}, function(error) {
							console.error('Error: ', error);
						});
					});
				}
			};

			profileCtrl.dataValid = function() {
				var email = profileCtrl.user.profile.email,
					password = profileCtrl.removePassword;

				return (email && password && password.length >= 6);
			};
        }
    ]);