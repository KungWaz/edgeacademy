/* jshint maxlen:200 */
angular.module('main.controller', [])
    .controller('MainCtrl', ['$rootScope', '$scope', '$location', 'Auth', 'Online',
        function($rootScope, $scope, $location, Auth, Online) {
            'use strict';

            var mainCtrl = this;

			$rootScope.scrollGlue = false;

			mainCtrl.onlineUserIds = [];
			mainCtrl.onlineUsers = Online.getOnlineUsers();

			mainCtrl.isLoggedIn = function() {
				return Auth.isLoggedIn();
			};

            mainCtrl.logOut = function() {
				Online.setOffline(Auth.user.uid);

				Auth.logOut();

				$location.path('sign-in');
            };

			$scope.$watch(function() {
				return mainCtrl.onlineUsers;
			}, function(newVal) {
				mainCtrl.onlineUserIds = [];

				for(var i = 0; i < newVal.length; i++) {
					mainCtrl.onlineUserIds.push(newVal[i].$id);
				}

				console.log('Online user IDs: ' + mainCtrl.onlineUserIds);
			}, true);
        }
    ]);