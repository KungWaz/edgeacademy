/* jshint maxlen:200 */
angular.module('register.controller', [])
    .controller('RegisterCtrl', ['$rootScope', '$location', '$analytics', 'Auth', 'currentAuth',
        function($rootScope, $location, $analytics, Auth, currentAuth) {
            'use strict';

            var registerCtrl = this;

			if(currentAuth !== null) {
				$location.path('chat');
			}

			$analytics.pageTrack('/register');

			$rootScope.scrollGlue = false;

            registerCtrl.user = Auth.user;

            registerCtrl.registerUser = function() {
				if(registerCtrl.dataValid() && !Auth.isLoggedIn()) {
					var username = registerCtrl.user.profile.username,
						email = registerCtrl.user.profile.email,
						password = registerCtrl.user.password;

					Auth.register({
						email: email,
						password: password
					}).then(function(userData) {
						console.log('User ' + userData.uid + ' created successfully!');

						$analytics.eventTrack('Register', {
							category: 'Register',
							label: 'E-mail - ' + email,
							value: email
						});

						return Auth.logIn({
							email: email,
							password: password
						});
					}).then(function(authData) {
						console.log('Logged in as:', authData.uid);

						$analytics.eventTrack('Sign in', {
							category: 'Sign in',
							label: 'User ID - ' + authData.uid,
							value: authData.uid
						});

						Auth.createProfile(authData.uid, {
							username: username,
							email: email
						}).then(function(ref) {
							console.log('Profile ' + ref.key() + ' successfully set!');

							$location.path('chat');
						}, function(error) {
							console.error('Error: ', error);
						});
					}).catch(function(error) {
						console.error('Error: ', error);
					});
				}
            };

			registerCtrl.dataValid = function() {
				if(!registerCtrl.user.profile) {
					return false;
				}

				var username = registerCtrl.user.profile.username,
					email = registerCtrl.user.profile.email,
					password = registerCtrl.user.password;

				return (username && username.length >= 3 && email && password && password.length >= 6);
			};
        }
    ]);