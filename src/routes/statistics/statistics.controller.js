/* jshint maxlen:200 */
/* global Chart */
angular.module('statistics.controller', [])
    .controller('StatisticsCtrl', ['$rootScope', '$scope', '$interval', '$firebase', '$analytics', 'Message',
        function($rootScope, $scope, $interval, $firebase, $analytics, Message) {
            'use strict';

            var statisticsCtrl = this,
                minutesPerStep = [1, 2, 6, 12],
                totalMinutes = [5, 10, 30, 60];

			$analytics.pageTrack('/statistics');

			$rootScope.scrollGlue = false;

            statisticsCtrl.stepVal = Message.getStepVal();
            statisticsCtrl.steps = [5, 5, 5, 5];
            statisticsCtrl.stepTitles = ['5 minute', '10 minutes', '30 minutes', '1 hour'];

            statisticsCtrl.labels = [];
            statisticsCtrl.series = ['Messages per minute'];
            statisticsCtrl.data = [[]];

            statisticsCtrl.messageCount = Message.getMessageCount();

			Chart.defaults.global.scaleBeginAtZero = true;

            function pad(num) {
                var p = '0';

                num = num + '';
                return num.length >= 2 ? num : new Array(2 - num.length + 1).join(p) + num;
            }

            function getTimeStampAsString(timeStamp) {
                if(!timeStamp) {
                    return '';
                }

                var dateObj = new Date(timeStamp),
                    hours = dateObj.getHours(),
                    minutes = dateObj.getMinutes();

                return pad(hours) + ':' + pad(minutes);
            }

            function triggerUpdate() {
                var dateObj = new Date(),
                    roundVal = 1000 * 60 * minutesPerStep[statisticsCtrl.stepVal],
                    currentTimeStamp = new Date(Math.round(dateObj.getTime() / roundVal) * roundVal - (1000 * 60 * totalMinutes[statisticsCtrl.stepVal])).getTime(),
                    totalCount = 0,
                    nextTimeStamp,
                    intervalCount,
                    tmpTimeStamp, i, j;

                statisticsCtrl.labels = [];
                statisticsCtrl.data = [[]];

                for(i = 0; i < statisticsCtrl.steps[statisticsCtrl.stepVal]; i++) {
                    currentTimeStamp += (1000 * 60 * minutesPerStep[statisticsCtrl.stepVal]);
                    nextTimeStamp = currentTimeStamp + (1000 * 60 * minutesPerStep[statisticsCtrl.stepVal]);
                    intervalCount = 0;

                    for(j = 0; j < statisticsCtrl.messageCount.length; j++) {
                        tmpTimeStamp = parseInt(statisticsCtrl.messageCount[j].$id, 10);

                        if(tmpTimeStamp >= currentTimeStamp && tmpTimeStamp <= nextTimeStamp) {
                            intervalCount += statisticsCtrl.messageCount[j].$value;
                            totalCount += statisticsCtrl.messageCount[j].$value;
                            break;
                        }
                    }

                    if(intervalCount) {
                        statisticsCtrl.labels.push(getTimeStampAsString(currentTimeStamp));
                        statisticsCtrl.data[0].push(intervalCount);
                    } else {
                        statisticsCtrl.labels.push(getTimeStampAsString(currentTimeStamp));
                        statisticsCtrl.data[0].push(0);
                    }
                }

                console.log('Total messages sent last ' + totalMinutes[statisticsCtrl.stepVal] + ' minutes: ' + totalCount);
            }

            statisticsCtrl.setStepVal = function() {
                $analytics.eventTrack('Statistics', {
                    category: 'History',
                    label: 'Time - ' + statisticsCtrl.stepTitles[statisticsCtrl.stepVal],
                    value: statisticsCtrl.stepTitles[statisticsCtrl.stepVal]
                });

                Message.setStepVal(statisticsCtrl.stepVal);
            };

            statisticsCtrl.messageCount.$watch(function() {
                triggerUpdate();
            });

            $scope.$watch(function() {
                return statisticsCtrl.stepVal;
            }, function() {
                triggerUpdate();
            });

			$interval(function() {
				triggerUpdate();
			}, 1000 * 60);
        }
    ]);