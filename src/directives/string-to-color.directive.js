angular.module('string-to-color.directive',[])
    .directive('stringToColor', function() {
		'use strict';

        return {
            restrict: 'A',
            scope: {
                inputStr: '='
            },
            link: function (scope, elem) {
                function getHashCode(string) {
                    var hash = 0;
                    if (string.length === 0) {
						return hash;
					}
                    for (var i = 0; i < string.length; i++) {
                        hash = string.charCodeAt(i) + ((hash << 5) - hash);
                        hash = hash & hash; // Convert to 32bit integer
                    }
                    return hash;
                }

                function intToHSL(int) {
                    var shortened = int % 360;
                    return 'hsl(' + shortened + ', 100%, 35%)';
                }

                elem.css({'color': intToHSL(getHashCode(scope.inputStr))});
            }
        };
    });