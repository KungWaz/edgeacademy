angular.module('timestamp-to-string.filter',[])
	.filter('toDateTimeString', function() {
		'use strict';

		return function(timeStamp) {
			var today = new Date(),
				dateObject = new Date(timeStamp ),
				months = ['jan', 'feb', 'mar', 'apr', 'mai', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];

			function pad(num) {
				var p = '0';

				num = num + '';
				return num.length >= 2 ? num : new Array(2 - num.length + 1).join(p) + num;
			}

			if(today.toDateString() === dateObject.toDateString()) {
				return pad(dateObject.getHours()) + ':' + pad(dateObject.getMinutes());
			} else {
				return 	dateObject.getDate() + ' ' +
						months[dateObject.getMonth() + 1] + ' ' +
						pad(dateObject.getHours()) + ':' +
						pad(dateObject.getMinutes());
			}
	     };
	});